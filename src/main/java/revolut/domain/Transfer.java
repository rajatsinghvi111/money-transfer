package revolut.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Transfer {
	
	private Long senderAccount;
	private Long receiverAccount;
	private Double amount;
	
}
