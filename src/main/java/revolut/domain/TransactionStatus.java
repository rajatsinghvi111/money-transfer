package revolut.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum TransactionStatus {
	
	SUCCESS(Constants.SUCCESS),
	FAILED(Constants.FAILED),
	PENDING(Constants.PENDING);
	
	private String transactionType;

	/*
	 * public static TransactionStatus get(String transactionType) {
	 * switch(transactionType) { case Constants.SUCCESS: return SUCCESS; case
	 * Constants.FAILED: return FAILED; case Constants.PENDING: return PENDING;
	 * default: return PENDING; } }
	 */
    
	private static class Constants {
        public static final String SUCCESS = "Success";
        public static final String FAILED = "Failed";
        public static final String PENDING = "Pending";
    }
}
