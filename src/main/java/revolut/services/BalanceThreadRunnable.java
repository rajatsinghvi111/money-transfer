package revolut.services;

import revolut.domain.Transfer;

public class BalanceThreadRunnable implements Runnable {

	private Transfer transfer;

	public BalanceThreadRunnable(Transfer transfer) {
		this.transfer = transfer;
	}

	@Override
	public void run() {
		try {
			TransactionService transactionService = new TransactionService();
			transactionService.transfer(transfer);
		} catch (Exception exception) {

		}

	}

}
