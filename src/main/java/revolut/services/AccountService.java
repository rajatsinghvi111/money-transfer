package revolut.services;

import java.sql.SQLException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import revolut.constant.Constants;
import revolut.domain.Account;
import revolut.exception.AccountException;
import revolut.repository.AccountRepository;

@Path("/account")
@Consumes({ "application/json"}) 
@Produces({ "application/json"})
public class AccountService {
	private AccountRepository accountRepository;

	public AccountService() {
		accountRepository = new AccountRepository();;
	}
	
	@POST
	public void create(Account account) {
		try {
			accountRepository.createAccount(account);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	@GET
	@Path("/{accountId}")
	public Account fetch(@PathParam("accountId") Long accountId) {
		try {
			return accountRepository.getAccountById(accountId);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@GET
	@Path("/all")
	public List<Account> fetchAll() {
		try {
			return accountRepository.getAllAccounts();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@PUT
	public Boolean update(Account account) {
		try {
			int updatedRows = accountRepository.updateAccountSpend(account);
			if(updatedRows > 0) return true;
			else throw new AccountException(Constants.ACCOUNT_NOT_EXISTS);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
}
