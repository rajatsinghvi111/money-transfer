package revolut.constant;

public class Constants {
	
	public interface DataBaseProperties {
		public static final String DB_DRIVER = "org.h2.Driver";
		public static final String DB_CONNECTION = "jdbc:h2:mem:mydb;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false;";
		public static final String DB_USER = "test";
		public static final String DB_PASSWORD = "test";
	}
	
	public static final String ACCOUNT_NOT_EXISTS = "Account does not exists";
	public static final String AMOUNT_NOT_VALID = "Transaction is failed, Transaction Amount is not valid";
	public static final String RECEIVER_ACCOUNT_NOT_VALID = "Transaction is failed, Receiver account is not valid";
	public static final String NOT_SUFFICIENT_BALANCE = "Transaction is failed, Not sufficient Balance";
	public static final String ACCOUNT_NOT_CREATED = "Account is not created successfully";
	public static final String ACCOUNT_NOT_FETCH = "Exception in fetching account from  DB";
	public static final String BALANCE_UPDATE_ERROR = "Exception in Updating balance";
}
