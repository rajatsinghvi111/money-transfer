package revolut;

import org.flywaydb.core.Flyway;

import revolut.constant.Constants;

public class Migration {
	public static void executeMigration() {
		System.out.println("Migration start");
		Flyway flyway = new Flyway();
		flyway.setDataSource(Constants.DataBaseProperties.DB_CONNECTION, Constants.DataBaseProperties.DB_USER,
				Constants.DataBaseProperties.DB_PASSWORD);
		flyway.setLocations("db/migration");
		flyway.migrate();
		System.out.println("Migration executed successfully");
	}
	
}
