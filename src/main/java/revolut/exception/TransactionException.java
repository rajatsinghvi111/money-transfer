package revolut.exception;

public class TransactionException extends RuntimeException {

	private static final long serialVersionUID = 643658736856843658L;

	public TransactionException(String reason) {
		super(reason);
	}
}
