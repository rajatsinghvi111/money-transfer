package revolut.services;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;
import java.util.Set;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.jodah.concurrentunit.ConcurrentTestCase;
import revolut.Application;
import revolut.Migration;
import revolut.constant.Constants;
import revolut.domain.Account;
import revolut.domain.Transfer;
import revolut.exception.TransactionException;

@RunWith(JUnit4.class)
public class TransactionServiceTest extends ConcurrentTestCase {
	
	private static AccountService accountService = new AccountService();
	private static TransactionService transactionService = new TransactionService();
	
	private static Account account1Actual;
	private static Account account2Actual;
	
	@BeforeClass
	public static void DBSetup() {
		Migration.executeMigration();
	}
	
	
	
	@Test
	public void whenAmountIsZero_shouldThrowAmountNotValidException() {
		try {
			createAccount();
			transactionService
					.transfer(new Transfer(account2Actual.getAccountNo(), account1Actual.getAccountNo(), 0.0));
		} catch (TransactionException exception) {
			assertEquals(Constants.AMOUNT_NOT_VALID, exception.getMessage());
		}
	}

	@Test
	public void whenAmountLessThanZero_shouldThrowAmountNotValidException() {
		try {
			createAccount();
			transactionService
					.transfer(new Transfer(account2Actual.getAccountNo(), account1Actual.getAccountNo(), -10000.0));
		} catch (TransactionException exception) {
			assertEquals(Constants.AMOUNT_NOT_VALID, exception.getMessage());
		}
	}

	@Test
	public void whenAmountGreaterThanBalance_shouldThrowNotSufficientBalanceException() {
		try {
			createAccount();
			transactionService
					.transfer(new Transfer(account2Actual.getAccountNo(), account1Actual.getAccountNo(), 50000.0));
		} catch (TransactionException exception) {
			assertEquals(Constants.NOT_SUFFICIENT_BALANCE, exception.getMessage());
		}
	}

	@Test
	public void whenTransferAmount_shouldThrowAccountNotExistsException() {
		try {
			createAccount();
			transactionService.transfer(new Transfer(account2Actual.getAccountNo(), 11113l, 5000.0));
		} catch (TransactionException exception) {
			assertEquals(Constants.ACCOUNT_NOT_EXISTS, exception.getMessage());
		}
	}

	@Test
	public void shouldThrowReceiverAccountNotValidException() {
		try {
			createAccount();
			transactionService
					.transfer(new Transfer(account2Actual.getAccountNo(), account2Actual.getAccountNo(), 50000.0));
		} catch (TransactionException exception) {
			assertEquals(Constants.RECEIVER_ACCOUNT_NOT_VALID, exception.getMessage());
		}
	}
	
	@Test
	public void ShouldTransferAmount() throws Throwable {
		createAccount();
		assertEquals(account1Actual.getBalance(), new Double(10000.0));
		assertEquals(account2Actual.getBalance(), new Double(20000.0));
		Account account3Actual = new Account(11113l, 30000.0);
		accountService.create(account3Actual);
		assertEquals(account3Actual.getBalance(), new Double(30000.0));
		
		new Thread(() -> {
			transactionService.transfer(new Transfer(account2Actual.getAccountNo(), account1Actual.getAccountNo(), 5000.0));
			
		}).start();
		
		new Thread(() -> {
			transactionService.transfer(new Transfer(account2Actual.getAccountNo(), account3Actual.getAccountNo(), 5000.0));
			
		}).start();
		
		new Thread(() -> {
			transactionService.transfer(new Transfer(account1Actual.getAccountNo(), account2Actual.getAccountNo(), 5000.0));
			
		}).start();
		
		new Thread(() -> {
			transactionService.transfer(new Transfer(account3Actual.getAccountNo(), account2Actual.getAccountNo(), 5000.0));
			
		}).start();
		
		Thread.sleep(5000);
		
		Account account1Expected = accountService.fetch(11111l);
		Account account2Expected = accountService.fetch(11112l);
		Account account3Expected = accountService.fetch(11113l);

		assertEquals(account1Expected.getBalance(), new Double(10000.0));
		assertEquals(account2Expected.getBalance(), new Double(20000.0));
		assertEquals(account3Expected.getBalance(), new Double(30000.0));
		 
	}
	
	private void createAccount() {
		account1Actual = new Account(11111l, 10000.0);
		account2Actual = new Account(11112l, 20000.0);
		
		accountService.create(account1Actual);
		accountService.create(account2Actual);
	}
	
	@After
	public void tearDown() {
		try {
			clearDatabase();
		} catch (Exception e) {

		}
	}

	public void clearDatabase() throws SQLException {
		Connection connection = Application.getDBConnection();
		Statement s = connection.createStatement();

		// Disable FK
		s.execute("SET REFERENTIAL_INTEGRITY FALSE");

		// Find all tables and truncate them
		Set<String> tables = new HashSet();
		ResultSet rs = s.executeQuery("SELECT TABLE_NAME FROM INFORMATION_SCHEMA.TABLES  where TABLE_SCHEMA='PUBLIC'");
		while (rs.next()) {
			tables.add(rs.getString(1));
		}
		rs.close();
		for (String table : tables) {
			s.executeUpdate("TRUNCATE TABLE " + table);
		}

		// Idem for sequences
		Set<String> sequences = new HashSet<String>();
		rs = s.executeQuery("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES WHERE SEQUENCE_SCHEMA='PUBLIC'");
		while (rs.next()) {
			sequences.add(rs.getString(1));
		}
		rs.close();
		for (String seq : sequences) {
			s.executeUpdate("ALTER SEQUENCE " + seq + " RESTART WITH 1");
		}

		// Enable FK
		s.execute("SET REFERENTIAL_INTEGRITY TRUE");
		s.close();
		connection.close();
	}
	
	
}
